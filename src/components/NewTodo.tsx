import React, { useRef } from "react";

interface todoProps {
  addTodo: (text: string) => void;
}

const NewTodo: React.FC<todoProps> = (props) => {
  const textInput = useRef<HTMLInputElement>(null);
  const handleSubmit = (e: React.FormEvent) => {
    e.preventDefault();
    const text = textInput.current!.value;
    props.addTodo(text);
  };
  return (
    <form onSubmit={handleSubmit}>
      <p>To do list</p>
      <input type="text" name="text" ref={textInput} />
      <button type="submit">Add</button>
    </form>
  );
};

export default NewTodo;
