import React from "react";

interface TodoProps {
  items: { id: string; text: string }[];
  deleteTodo: (id: string) => void;
}

const Todos: React.FC<TodoProps> = (props) => {
  return (
    <ul>
      {props.items.map((item) => (
        <li key={item.id}>
          <span>{item.text}</span>
          <button onClick={props.deleteTodo.bind(null, item.id)}>Delete</button>
        </li>
      ))}
    </ul>
  );
};

export default Todos;
