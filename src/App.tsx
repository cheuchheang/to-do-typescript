import React, { useState } from "react";
import Todos from "./components/Todos";
import NewTodo from "./components/NewTodo";

function App() {
  const [todos, setTodos] = useState<{ id: string; text: string }[]>([]);

  const addTodo = (text: string) => {
    setTodos((prevState) => [
      ...prevState,
      { id: Math.random().toString(), text: text },
    ]);
  };

  const deleteTodo = (todoId: string) => {
    setTodos((prevState) => {
      return prevState.filter((item) => item.id !== todoId);
    });
  };
  return (
    <div className="App">
      <NewTodo addTodo={addTodo} />
      <Todos items={todos} deleteTodo={deleteTodo} />
    </div>
  );
}

export default App;
